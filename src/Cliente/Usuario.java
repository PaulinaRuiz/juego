package Cliente;

import InicioSesion.OyenteUsuario;
import InicioSesion.OyenteUsuario;



public class Usuario extends javax.swing.JPanel {

    public Usuario() {
        initComponents();
    }
    
    public void addEventos(OyenteUsuario oyente){
        botonCancelar.addActionListener(oyente);
        botonIniciar.addActionListener(oyente);
        botonNuevaCuenta.addActionListener(oyente);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelTitulo = new javax.swing.JPanel();
        tituloLogin = new javax.swing.JLabel();
        panelDatos = new javax.swing.JPanel();
        labelUsuario = new javax.swing.JLabel();
        textUsuario = new javax.swing.JTextField();
        labelContrasena = new javax.swing.JLabel();
        textPassword = new javax.swing.JPasswordField();
        panelBotones = new javax.swing.JPanel();
        botonIniciar = new javax.swing.JButton();
        botonNuevaCuenta = new javax.swing.JButton();
        botonCancelar = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 0, 204));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 0, 102), 2));
        setForeground(new java.awt.Color(102, 0, 102));
        setPreferredSize(new java.awt.Dimension(380, 150));

        panelTitulo.setBackground(getBackground());

        tituloLogin.setFont(new java.awt.Font("SansSerif", 1, 18)); // NOI18N
        tituloLogin.setForeground(new java.awt.Color(254, 254, 254));
        tituloLogin.setText("Iniciar sesión");

        javax.swing.GroupLayout panelTituloLayout = new javax.swing.GroupLayout(panelTitulo);
        panelTitulo.setLayout(panelTituloLayout);
        panelTituloLayout.setHorizontalGroup(
            panelTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTituloLayout.createSequentialGroup()
                .addGap(141, 141, 141)
                .addComponent(tituloLogin))
        );
        panelTituloLayout.setVerticalGroup(
            panelTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTituloLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(tituloLogin))
        );

        panelDatos.setBackground(getBackground());
        panelDatos.setPreferredSize(new java.awt.Dimension(200, 80));

        labelUsuario.setFont(new java.awt.Font("SansSerif", 1, 15)); // NOI18N
        labelUsuario.setForeground(new java.awt.Color(254, 254, 254));
        labelUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelUsuario.setText("Usuario:");
        labelUsuario.setPreferredSize(new java.awt.Dimension(100, 30));

        textUsuario.setColumns(13);
        textUsuario.setFont(new java.awt.Font("SansSerif", 0, 15)); // NOI18N

        labelContrasena.setFont(new java.awt.Font("SansSerif", 1, 15)); // NOI18N
        labelContrasena.setForeground(new java.awt.Color(254, 254, 254));
        labelContrasena.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labelContrasena.setText("Contraseña:");
        labelContrasena.setPreferredSize(new java.awt.Dimension(100, 30));

        textPassword.setColumns(13);
        textPassword.setFont(new java.awt.Font("SansSerif", 0, 15)); // NOI18N

        javax.swing.GroupLayout panelDatosLayout = new javax.swing.GroupLayout(panelDatos);
        panelDatos.setLayout(panelDatosLayout);
        panelDatosLayout.setHorizontalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addComponent(labelUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(textUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addComponent(labelContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(textPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        panelDatosLayout.setVerticalGroup(
            panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelDatosLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(textUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addGroup(panelDatosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelContrasena, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelDatosLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(textPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        panelBotones.setBackground(getBackground());
        panelBotones.setPreferredSize(new java.awt.Dimension(380, 40));

        botonIniciar.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        botonIniciar.setText("Iniciar Sesion");
        botonIniciar.setName("iniciar"); // NOI18N
        botonIniciar.setPreferredSize(new java.awt.Dimension(120, 30));

        botonNuevaCuenta.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        botonNuevaCuenta.setText("Crear cuenta");
        botonNuevaCuenta.setName("nuevo"); // NOI18N
        botonNuevaCuenta.setPreferredSize(new java.awt.Dimension(120, 30));

        botonCancelar.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        botonCancelar.setText("Cancelar");
        botonCancelar.setName("cancelar"); // NOI18N
        botonCancelar.setPreferredSize(new java.awt.Dimension(100, 30));

        javax.swing.GroupLayout panelBotonesLayout = new javax.swing.GroupLayout(panelBotones);
        panelBotones.setLayout(panelBotonesLayout);
        panelBotonesLayout.setHorizontalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(botonIniciar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(botonNuevaCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(botonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panelBotonesLayout.setVerticalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonIniciar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonNuevaCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(panelDatos, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(panelBotones, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelDatos, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonCancelar;
    private javax.swing.JButton botonIniciar;
    private javax.swing.JButton botonNuevaCuenta;
    private javax.swing.JLabel labelContrasena;
    private javax.swing.JLabel labelUsuario;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelDatos;
    private javax.swing.JPanel panelTitulo;
    private javax.swing.JPasswordField textPassword;
    private javax.swing.JTextField textUsuario;
    private javax.swing.JLabel tituloLogin;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the textPassword
     */
    public javax.swing.JPasswordField getTextPassword() {
        return textPassword;
    }

    /**
     * @return the textUsuario
     */
    public javax.swing.JTextField getTextUsuario() {
        return textUsuario;
    }
}
