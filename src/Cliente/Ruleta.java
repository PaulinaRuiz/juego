package Cliente;

import InicioSesion.Conexion;
import java.awt.Color;
import java.util.Random;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Ruleta extends javax.swing.JFrame {

    private final ImageIcon icons[] = new ImageIcon[3];
    private final String images[] = {"cerezas.png", "manzanas.png", "fresas.png"};
    private final Color colores[] = {new Color(132,176,217), new Color(223,124,165), new Color(114,241,92)};
    private final int indexes[] = new int[3];
    private final int giros[] = new int[3];
    Random random = new Random();
    Vector<JLabel> labels = new Vector<>();
    private final String user;
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelCabecera = new javax.swing.JPanel();
        etiquetaCasino = new javax.swing.JLabel();
        panelImagenes = new javax.swing.JPanel();
        etiquetaImage1 = new javax.swing.JLabel();
        etiquetaImage2 = new javax.swing.JLabel();
        etiquetaImage3 = new javax.swing.JLabel();
        panelEtiquetas = new javax.swing.JPanel();
        panelCreditos = new javax.swing.JPanel();
        valorCreditos = new javax.swing.JLabel();
        etiquetaCreditos = new javax.swing.JLabel();
        panelApuesta = new javax.swing.JPanel();
        valorApuesta = new javax.swing.JLabel();
        etiquetaApuesta = new javax.swing.JLabel();
        panelBotones = new javax.swing.JPanel();
        botonInsertar = new javax.swing.JButton();
        botonRetirar = new javax.swing.JButton();
        botonApostar = new javax.swing.JButton();
        botonJugar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Juego de Casino");
        setBackground(new java.awt.Color(255, 102, 255));
        setPreferredSize(new java.awt.Dimension(450, 555));

        panelCabecera.setBackground(new java.awt.Color(255, 51, 204));
        panelCabecera.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 153), 5));
        panelCabecera.setPreferredSize(new java.awt.Dimension(400, 100));

        etiquetaCasino.setBackground(panelCabecera.getBackground());
        etiquetaCasino.setFont(new java.awt.Font("Freestyle Script", 1, 48)); // NOI18N
        etiquetaCasino.setForeground(new java.awt.Color(254, 254, 254));
        etiquetaCasino.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaCasino.setText("RULETILLA");
        etiquetaCasino.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 0, 204), 5, true));
        etiquetaCasino.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        etiquetaCasino.setOpaque(true);
        etiquetaCasino.setPreferredSize(new java.awt.Dimension(370, 80));

        javax.swing.GroupLayout panelCabeceraLayout = new javax.swing.GroupLayout(panelCabecera);
        panelCabecera.setLayout(panelCabeceraLayout);
        panelCabeceraLayout.setHorizontalGroup(
            panelCabeceraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCabeceraLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(etiquetaCasino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panelCabeceraLayout.setVerticalGroup(
            panelCabeceraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCabeceraLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(etiquetaCasino, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelImagenes.setBackground(new java.awt.Color(255, 0, 255));
        panelImagenes.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 153), 5));
        panelImagenes.setPreferredSize(new java.awt.Dimension(400, 170));

        etiquetaImage1.setBackground(new java.awt.Color(132, 176, 217));
        etiquetaImage1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaImage1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/cerezas.png"))); // NOI18N
        etiquetaImage1.setOpaque(true);

        etiquetaImage2.setBackground(new java.awt.Color(223, 124, 165));
        etiquetaImage2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaImage2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/fresas.png"))); // NOI18N
        etiquetaImage2.setOpaque(true);

        etiquetaImage3.setBackground(new java.awt.Color(114, 241, 92));
        etiquetaImage3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaImage3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/manzanas.png"))); // NOI18N
        etiquetaImage3.setOpaque(true);

        javax.swing.GroupLayout panelImagenesLayout = new javax.swing.GroupLayout(panelImagenes);
        panelImagenes.setLayout(panelImagenesLayout);
        panelImagenesLayout.setHorizontalGroup(
            panelImagenesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImagenesLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(etiquetaImage1)
                .addGap(5, 5, 5)
                .addComponent(etiquetaImage2)
                .addGap(5, 5, 5)
                .addComponent(etiquetaImage3))
        );
        panelImagenesLayout.setVerticalGroup(
            panelImagenesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelImagenesLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(etiquetaImage1))
            .addGroup(panelImagenesLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(etiquetaImage2))
            .addGroup(panelImagenesLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(etiquetaImage3))
        );

        panelEtiquetas.setBackground(new java.awt.Color(255, 0, 204));
        panelEtiquetas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 0, 153), 5));
        panelEtiquetas.setPreferredSize(new java.awt.Dimension(400, 100));

        panelCreditos.setBackground(panelEtiquetas.getBackground());
        panelCreditos.setPreferredSize(new java.awt.Dimension(180, 80));

        valorCreditos.setFont(new java.awt.Font("SansSerif", 0, 20)); // NOI18N
        valorCreditos.setForeground(new java.awt.Color(254, 254, 254));
        valorCreditos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        valorCreditos.setText("1000");
        valorCreditos.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 0, 153), 3, true));
        valorCreditos.setPreferredSize(new java.awt.Dimension(150, 40));

        etiquetaCreditos.setBackground(etiquetaCreditos.getBackground());
        etiquetaCreditos.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etiquetaCreditos.setForeground(new java.awt.Color(254, 254, 254));
        etiquetaCreditos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaCreditos.setText("CRÉDITOS");
        etiquetaCreditos.setPreferredSize(new java.awt.Dimension(150, 30));

        javax.swing.GroupLayout panelCreditosLayout = new javax.swing.GroupLayout(panelCreditos);
        panelCreditos.setLayout(panelCreditosLayout);
        panelCreditosLayout.setHorizontalGroup(
            panelCreditosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCreditosLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(panelCreditosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(valorCreditos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaCreditos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        panelCreditosLayout.setVerticalGroup(
            panelCreditosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCreditosLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(valorCreditos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(etiquetaCreditos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        panelApuesta.setBackground(panelEtiquetas.getBackground());
        panelApuesta.setPreferredSize(new java.awt.Dimension(180, 80));

        valorApuesta.setFont(new java.awt.Font("SansSerif", 0, 20)); // NOI18N
        valorApuesta.setForeground(new java.awt.Color(254, 254, 254));
        valorApuesta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        valorApuesta.setText("0");
        valorApuesta.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153, 0, 153), 3, true));
        valorApuesta.setPreferredSize(new java.awt.Dimension(150, 40));

        etiquetaApuesta.setBackground(etiquetaCreditos.getBackground());
        etiquetaApuesta.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        etiquetaApuesta.setForeground(new java.awt.Color(254, 254, 254));
        etiquetaApuesta.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        etiquetaApuesta.setText("APUESTA");
        etiquetaApuesta.setPreferredSize(new java.awt.Dimension(150, 30));

        javax.swing.GroupLayout panelApuestaLayout = new javax.swing.GroupLayout(panelApuesta);
        panelApuesta.setLayout(panelApuestaLayout);
        panelApuestaLayout.setHorizontalGroup(
            panelApuestaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelApuestaLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(panelApuestaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(valorApuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaApuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        panelApuestaLayout.setVerticalGroup(
            panelApuestaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelApuestaLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(valorApuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(etiquetaApuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout panelEtiquetasLayout = new javax.swing.GroupLayout(panelEtiquetas);
        panelEtiquetas.setLayout(panelEtiquetasLayout);
        panelEtiquetasLayout.setHorizontalGroup(
            panelEtiquetasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEtiquetasLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(panelCreditos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelApuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panelEtiquetasLayout.setVerticalGroup(
            panelEtiquetasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEtiquetasLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(panelEtiquetasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelCreditos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelApuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        panelBotones.setBackground(getBackground());
        panelBotones.setPreferredSize(new java.awt.Dimension(400, 90));

        botonInsertar.setBackground(panelBotones.getBackground());
        botonInsertar.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        botonInsertar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/diamantes.png"))); // NOI18N
        botonInsertar.setText("Insertar");
        botonInsertar.setBorderPainted(false);
        botonInsertar.setContentAreaFilled(false);
        botonInsertar.setFocusPainted(false);
        botonInsertar.setFocusable(false);
        botonInsertar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonInsertar.setName("insertar"); // NOI18N
        botonInsertar.setPreferredSize(new java.awt.Dimension(90, 80));
        botonInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonInsertarActionPerformed(evt);
            }
        });

        botonRetirar.setBackground(panelBotones.getBackground());
        botonRetirar.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        botonRetirar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/diamantes.png"))); // NOI18N
        botonRetirar.setText("Retirar");
        botonRetirar.setBorderPainted(false);
        botonRetirar.setContentAreaFilled(false);
        botonRetirar.setFocusPainted(false);
        botonRetirar.setFocusable(false);
        botonRetirar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonRetirar.setName("retirar"); // NOI18N
        botonRetirar.setPreferredSize(new java.awt.Dimension(90, 80));
        botonRetirar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRetirarActionPerformed(evt);
            }
        });

        botonApostar.setBackground(panelBotones.getBackground());
        botonApostar.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        botonApostar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/diamantes.png"))); // NOI18N
        botonApostar.setText("Apuesta");
        botonApostar.setBorderPainted(false);
        botonApostar.setContentAreaFilled(false);
        botonApostar.setFocusPainted(false);
        botonApostar.setFocusable(false);
        botonApostar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonApostar.setName("apostar"); // NOI18N
        botonApostar.setPreferredSize(new java.awt.Dimension(90, 80));
        botonApostar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonApostarActionPerformed(evt);
            }
        });

        botonJugar.setBackground(panelBotones.getBackground());
        botonJugar.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        botonJugar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/diamantes.png"))); // NOI18N
        botonJugar.setText("Jugar");
        botonJugar.setBorderPainted(false);
        botonJugar.setContentAreaFilled(false);
        botonJugar.setFocusPainted(false);
        botonJugar.setFocusable(false);
        botonJugar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        botonJugar.setName("jugar"); // NOI18N
        botonJugar.setPreferredSize(new java.awt.Dimension(90, 80));
        botonJugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonJugarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelBotonesLayout = new javax.swing.GroupLayout(panelBotones);
        panelBotones.setLayout(panelBotonesLayout);
        panelBotonesLayout.setHorizontalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(botonInsertar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(botonRetirar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(botonApostar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(botonJugar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        panelBotonesLayout.setVerticalGroup(
            panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBotonesLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(panelBotonesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonInsertar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonRetirar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonApostar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonJugar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelEtiquetas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelCabecera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelImagenes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelEtiquetas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelImagenes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelCabecera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelBotones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonApostarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonApostarActionPerformed
        String apuesta = JOptionPane.showInputDialog("Introduce la apuesta: ");
        if(apuesta != null)
             valorApuesta.setText(apuesta);
        else valorApuesta.setText("0");
    }//GEN-LAST:event_botonApostarActionPerformed

    private void botonInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonInsertarActionPerformed
        String creditos = JOptionPane.showInputDialog("Ingresar créditos:");
        if(creditos == null)
            return;
        
        int total = Integer.parseInt(creditos);
        int nTotal = Integer.parseInt(this.valorCreditos.getText());
        nTotal += total;
        Conexion cliente = new Conexion();
        cliente.enviarMensaje("actualizaCreditos " + user + " " + nTotal);
        valorCreditos.setText(Integer.toString(nTotal));
    }//GEN-LAST:event_botonInsertarActionPerformed

    private void botonRetirarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRetirarActionPerformed
        String reclamar = JOptionPane.showInputDialog("Retirar créditos:");
        
        if(reclamar == null)
            return;
        
        int retirar = Integer.parseInt(reclamar);
        int creditos = Integer.parseInt(valorCreditos.getText());

        if(retirar > creditos){
            JOptionPane.showOptionDialog(null, "No cuenta con suficientes créditos", "", 
            JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
        }else{
            creditos -= retirar;
            valorCreditos.setText(creditos + "");
            Conexion cliente = new Conexion();
            cliente.enviarMensaje("actualizaCreditos " + user + " " + creditos);
        }
    }//GEN-LAST:event_botonRetirarActionPerformed

    private void botonJugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonJugarActionPerformed
        
        int apuesta = Integer.parseInt(valorApuesta.getText());
        int creditos = Integer.parseInt(valorCreditos.getText());
        if(creditos >= apuesta){
            jugar();
        }else{
            JOptionPane.showMessageDialog(null, "Necesita mas creditos para jugar.");
        }
    }//GEN-LAST:event_botonJugarActionPerformed
public Ruleta(String user, String creditos) {
        getContentPane().setBackground(Color.BLACK);
        initComponents();
        this.user = user;
        this.valorCreditos.setText(creditos);
        
        for(int i=0; i<3; i++){
            indexes[i] = -1;
            icons[i] = (new ImageIcon(this.getClass().getResource("/resources/" + images[i])));
        }
        
        labels.add(etiquetaImage1);
        labels.add(etiquetaImage2);
        labels.add(etiquetaImage3);
        
    }
    
    public void jugar(){
        for(int i=0; i<3; i++)
            giros[i] = random.nextInt((50 - 20) + 1) + 20;
        
        Timer tiempo = new Timer(100, null);
        tiempo.addActionListener((e) -> {
            
            boolean band = true;
            
            for(int i=0; i<3; i++){
                if(giros[i] == 0)
                    continue;
                band = false;
                giros[i]--;
                indexes[i] = (indexes[i] + 1) % 3;
                labels.get(i).setIcon(icons[indexes[i]]);
                labels.get(i).setBackground(colores[indexes[i]]);
            }
            
            if(band){
                checarResultado();
                tiempo.stop();
            }
            else tiempo.restart();
        });

        tiempo.start();
    }
    
    public void checarResultado(){
        
        int apuesta  = Integer.parseInt(valorApuesta.getText());
        int creditos = Integer.parseInt(valorCreditos.getText());
        Conexion cliente = new Conexion();
        
        if(indexes[0] == indexes[1] && indexes[1] == indexes[2]){
            apuesta *= 2;
            creditos += apuesta;
            cliente.enviarMensaje("NuevoJuego " + user + " " + creditos + " Gana");
            etiquetaCasino.setForeground(Color.BLUE);
            etiquetaCasino.setText("GANASTE!!!!!");
        }
        else if(indexes[0] == indexes[1] || indexes[0] == indexes[2] || indexes[1] == indexes[2]){
            apuesta /= 2;
            creditos += apuesta;
            cliente.enviarMensaje("NuevoJuego " + user + " " + creditos + " Gana");
            etiquetaCasino.setForeground(Color.BLUE);
            etiquetaCasino.setText("GANASTE!!!!!");
        }
        else {
            creditos -= apuesta;
            cliente.enviarMensaje("NuevoJuego " + user + " " + creditos + " Pierde");
            etiquetaCasino.setForeground(Color.RED);
            etiquetaCasino.setText(":'C PERDISTE!!!");
        }
        valorCreditos.setText(Long.toString(creditos));
        cliente = new Conexion();
        cliente.enviarMensaje("actualizaCreditos " + user + " " + creditos);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonApostar;
    private javax.swing.JButton botonInsertar;
    private javax.swing.JButton botonJugar;
    private javax.swing.JButton botonRetirar;
    private javax.swing.JLabel etiquetaApuesta;
    private javax.swing.JLabel etiquetaCasino;
    private javax.swing.JLabel etiquetaCreditos;
    private javax.swing.JLabel etiquetaImage1;
    private javax.swing.JLabel etiquetaImage2;
    private javax.swing.JLabel etiquetaImage3;
    private javax.swing.JPanel panelApuesta;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelCabecera;
    private javax.swing.JPanel panelCreditos;
    private javax.swing.JPanel panelEtiquetas;
    private javax.swing.JPanel panelImagenes;
    private javax.swing.JLabel valorApuesta;
    private javax.swing.JLabel valorCreditos;
    // End of variables declaration//GEN-END:variables
}
